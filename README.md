<img src="./comprise_client_library.png" width="200" height="200">

![Maintained][maintained-badge]
[![Travis Build Status][build-badge]][build]
[![Make a pull request][prs-badge]][prs]
[![License](http://img.shields.io/badge/Licence-MIT-brightgreen.svg)](LICENSE.md)

[![Tweet][twitter-badge]][twitter]


All of the functionality below is implemented as an example in a [COMPRISE Sample App](https://gitlab.inria.fr/comprise/comprise_sample_app), in more detail [here](https://gitlab.inria.fr/comprise/comprise_sample_app/-/blob/master/src/app/home/home.page.ts)

- The COMPRISE Client Library should be installed after the [COMPRISE Personal Server](https://gitlab.inria.fr/comprise/comprise-personal-server) and after the [COMPRISE App Wizard](https://gitlab.inria.fr/comprise/comprise_app_wizard).
An installation & usage overview video can be found on YouTube [here](https://www.youtube.com/watch?v=ZqNcCc-tdBk).

# Operation Branch

## Speech-To-Text

Transforms user's vocal input into native textual representation.

### startRecording

Currently using [RecordRTC](https://www.npmjs.com/package/recordrtc) to start recording users voice with 16000 sample rate (16khz),
audio/wav, one audio channel.

#### Parameters

none

#### Callback

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **success**  | **boolean**  | Success of started operation.  | 

 

#### Example

``` js
import { startRecording } from 'comprise_speech_to_text'
import { HTTP } from '@ionic-native/http/ngx';

...

constructor(public http: HTTP) {

	...
	
	startRecording().then(() => {
		console.log("Start talking to me until I stop");
	},(onerror) => {
		console.log("Following error happened :"+onerror);
	}); 
}

```

## stopRecording

Terminates the stopped record and sends the recorded audio file to the personalized server to be processed, it will return the resulting, textual answer.
Will use language parameterto determine which model to use by the personalized server.
### Parameters

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **language**  | **string**  | The language key, **format "de", "en"**, ... . | 
| **http**  | **any**  | The [native HTTP Plugin from Android or iOS](https://github.com/silkimen/cordova-plugin-advanced-http).  | 
 

### Callback

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **text**  | **string**  | Textual representation of the voice input.  | 
 
### Example

``` js
import { stopRecording } from 'comprise_speech_to_text'
import { HTTP } from '@ionic-native/http/ngx';

...

constructor(public http: HTTP) {

	...
	
	stopRecording(this.privateStorage.langShort, http).then((text) => {
		console.log("Start talking to me until I stop " + text.text + " audio " + text.audio);
	},(onerror) => {
		console.log("Following error happened: "+onerror);
	});  
}

```


## streamRecording

streamRecording is a method that don't needs to be stopped by a second method. It gives you an event emitter with 3 events to listen to: "SPEAKING" = User is speaking, "TRANSFERRRING" = User input is transferred to server, "STOPPED" = User input is done and available as string. Also the input "beep" sounds can be disabled, which helps if you want to have an activation word and you want to listen silently for it.  

### Parameters

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **language**  | **string**  | The language key, **format "de", "en"**, ... . | 
| **http**  | **any**  | The [native HTTP Plugin from Android or iOS](https://github.com/silkimen/cordova-plugin-advanced-http).  | 
| **disableAudio**  | **boolean**  | Optional param. Set true to disable the recording beep sound.   | 

 

### Callback

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **emitter**  | **EventEmitter**  | EventEmitter that holds the events of the voice input "SPEAKING", "TRANSFERRING", "STOPPED"   | 
 
### Example

``` js
import { streamRecording } from 'comprise_speech_to_text'
import { HTTP } from '@ionic-native/http/ngx';

...

constructor(public http: HTTP) {

	...
	streamRecording('en', this.http, true).then((emitter: any) => {
		emitter.on("SPEAKING", (() => {
			console.log("You could show some indicator, that the recording happens");
		});
		emitter.on('STOPPED', (text: any) => {
			console.log("text contains the speech input string and audio: " + text.text + " " + text.audio);
		});
		emitter.on('TRANSFERRING', (text: string) => {
			console.log("Show a load indicator, or something");
			
		});
	});
}

```


## Machine Translation

Will translate the textual user input in native language into a pivot language and backwards. 

Currently uses [LetsMT](https://www.letsmt.eu/) for following translations:

- LV-EN
- EN-LV
- EN-DE

Other translations are handeled by [Yandex](https://www.yandex.com/) (temporarily), as soon as more languages are supported by [LetsMT](https://www.letsmt.eu/).
The same language for both native and pivot language will cause a simple return of the text without changes.

### translate

Will translate one language into an other.

#### Parameters

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **sentence**  | **string**  | The text to be translated | 
| **from**  | **string**  | The language key of the current language, **format "de", "en"**, ... . | 
| **to**  | **string**  | The language key of the language to translate to, **format "de", "en"**, ... . | 
| **letsMtID**  | **string**  | The client ID of [LetsMT](https://www.letsmt.eu/), in case it can be used. | 
| **yandexID**  | **string**  | The client ID of [Yandex](https://www.yandex.com/), in case it needs to be used. | 
| **http**  | **any**  | The [native HTTP Plugin from Android or iOS](https://github.com/silkimen/cordova-plugin-advanced-http).  | 
 
#### Callback

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **translation**  | **string**  | Translated text.  | 

#### Example

``` js
import { translate } from 'comprise_machine_translation'
import { HTTP } from '@ionic-native/http/ngx';

...

constructor(public http: HTTP) {

	...
	
	translate("Hey, translate me please", "en", "de", "XXXXXXXXX", "XXXXXXXXX", http).then((translation) => {
		console.log("My translation is: "+translation); 
	},(onerror) => {
		console.log("Following error happened: "+onerror);
	});  
}

```

## Spoken Language Understanding

Will take a text and interpretes the intent of the user.

Currently uses API from [Tilde.AI](https://botdashboard.tilde.ai/Intents) for accessing possible intents.

For training of those intents use [COMPRISE App Wizard](https://gitlab.inria.fr/comprise/comprise_app_wizard).

Directly accessed without personalized server.

### detectIntent

Will take a text and interpretes the intent of the user.

#### Parameters

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **text**  | **string**  | The text to be interpreted for the intent | 
| **appId**  | **string**  | ID/Name of the your application. | 
| **langId**  | **string**  | The language key of the language to translate to, **format "de-de", "en-en"**, ... .  | 
| **apiKey**  | **string**  | The API Key to access TILDE.AI. | 
| **http**  | **any**  | The [native HTTP Plugin from Android or iOS](https://github.com/silkimen/cordova-plugin-advanced-http).  | 
 
#### Callback

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **intent**  | **string**  | The detected intent.  | 

#### Example

``` js
import { detectIntent } from 'comprise_natural_language_understanding'
import { HTTP } from '@ionic-native/http/ngx';

...

constructor(public http: HTTP) {

	...
	
	detectIntent(text, "myCOMPRISEApp", "en-en", "XXXXXXXXX", http).then((intent) => {
		console.log("Detected intent is: "+intent)
	},(onerror) => {
		console.log("Following error happened: "+onerror);
	});
}

```


## Dialog Management / Spoken Language Generation

With the detected intent of the user, the current communication scenario will be re-evaluated. 
It will be decided which route to go next within that scenario (Dialog Management).
Based on that decision, Spoken Language Generation will create a suitable, text-based answer.
This module combines both components.

Currently uses API from Tilde.AI for both [Dialog Management](https://botdashboard.tilde.ai/Model) and 
[Spoken Language Generation](https://botdashboard.tilde.ai/Lang). 

For training of dialog scenarios and related answers use [COMPRISE App Wizard](https://gitlab.inria.fr/comprise/comprise_app_wizard)

Directly accessed without personalized server.

### openConversation

Will open a new conversation between the user and a chatbot, including a fresh communication scenario.

#### Parameters

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **apiKey**  | **string**  | The API Key to access TILDE.AI. | 
| **http**  | **any**  | The [native HTTP Plugin from Android or iOS](https://github.com/silkimen/cordova-plugin-advanced-http).  | 
 
#### Callback

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **conversation**  | **object**  | A "conversation"-object with attributes below | 
| **conversation.token**  | **object**  | Token of chosen communication. | 
| **conversation.conversationId**  | **object**  | ID of chosen communication. | 

#### Example

``` js
import { openConversation } from 'comprise_natural_language_generation'
import { HTTP } from '@ionic-native/http/ngx';

...

constructor(public http: HTTP) {

	...
	
	openConversation("XXXXXXXXXXX", http).then((conversation) => {
		console.log("Conversation object is: " + conversation);
		console.log("Conversation token is: " + conversation.token + " and the ID: " + conversation.conversationId);
	},(onerror) => {
		console.log("Following error happened: "+onerror);
	});
}

```

### retrieveConversation

Will retrieve a conversation already started in the past between the user and a chatbot, including the current communication scenario at that time.

#### Parameters

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **conversationId**  | **string**  | The Id of the conversation we want to retrieve. | 
| **apiKey**  | **string**  | The API Key to access TILDE.AI. | 
| **http**  | **any**  | The [native HTTP Plugin from Android or iOS](https://github.com/silkimen/cordova-plugin-advanced-http).  | 
 
#### Callback

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **conversation**  | **object**  | A "conversation"-object with attributes "token" and "conversationId". | 

#### Example

``` js
import { retrieveConversation } from 'comprise_natural_language_generation'
import { HTTP } from '@ionic-native/http/ngx';

...

constructor(public http: HTTP) {

	...
	
	retrieveConversation("123abc", "XXXXXXXXXXX", http).then((conversation) => {
		console.log("Conversation object is: " + conversation);
		console.log("Conversation token is: " + conversation.token + " and the ID: " + conversation.conversationId);
	},(onerror) => {
		console.log("Following error happened: "+onerror);
	});
}

```


### generateAnswerToMessage

Will send an intent / a message to the chatbot of the chosen communication and sends back the suitable answer.

#### Parameters

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **conversationId**  | **string**  | The Id of the chosen communication. | 
| **intent**  | **string**  | The message / the intent to communicate. | 
| **API key** | **string**  | The API Key. | 
| **http**  | **any**  | The [native HTTP Plugin from Android or iOS](https://github.com/silkimen/cordova-plugin-advanced-http).  | 
 
#### Callback

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **answer**  | **object**  | The answer to the sent message. | 

#### Example

``` js
import { generateAnswerToMessage } from 'comprise_natural_language_generation'
import { HTTP } from '@ionic-native/http/ngx';

...

constructor(public http: HTTP) {

	...
	
	generateAnswerToMessage("123abc", "hungry", "XXXXXXXXXXX", http).then((answer) => {
		console.log("Your answer is: "+answer);
	},(onerror) => {
		console.log("Following error happened: "+onerror);
	});
}

```


### WebSocket interface

The methods above use HTTP calls to use the natural language generation. But there is also an interface to communicate over WebSocket. This is slightly harder to use but comes with two advantages: You can retrieve broken connections and get messages that have been lost and due to the HTTP overhead it's better in terms of performance. 

#### Parameters

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **apiKey**  | **string**  | The API Key to access TILDE.AI. | 
 
#### Return value

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **directLine**  | **DirectLine**  | The object to interact with the NLG server | 

#### Example

``` js
import { createWebSocketConversation } from 'comprise_natural_language_generation'

...

constructor(public http: HTTP) {

	...

	const directLine = createWebSocketConversation('<API SECRET HERE>'); 
	directLine.postActivity({
		from: { id: 'Some user id', name: 'Andre from Ascora' },
		type: 'message',
		text: 'This is my test'
	}).subscribe(
		id => alert("Activity ID: " + id),
		error => alert(JSON.stringify(error))
	);

	directLine.activity$
	.subscribe(
		activity => console.log("received activity ", activity)
	);
}

```

This method returns a DirectLine object which is a wrapper for the offical API SDK of Microsoft. So you can use everything that is supported by the SDK. Full documentation on this can be found here: [botframework-directlinejs](https://www.npmjs.com/package/botframework-directlinejs)


## Text-To-Speech

Transforms textual representation into voice, which will be spoken out.

Currently using [Cordova](https://cordova.apache.org/) plugin as solution to run native functionality on device, connected to Angular / Web-Code.

### speak

Transforms textual representation into voice, which will be spoken out.

#### Parameters

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **tts_options**  | **object**  | Configuration object with parameters below.  | 
| **tts_options.text**  | **string**  | Text to be spoken out.  | 
| **tts_options.locale**  | **string**  | The language used, **format "de-DE", "en-EN"**, ... .  | 

#### Callback

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **success**  | **boolean**  | Success of started operation.  | 

#### Example

``` js
declare var cordova: any;

...

constructor() {

	...
	
	const tts_options = {"text": "Please talk to me!", "locale": "en-EN"};
	cordova.plugins.COMPRISE_TextToSpeech.speak(tts_options, () => {
		console.log('Success')
	},(onerror) => {
		console.log("Following error happened :"+onerror);
	}); 
}

```


# Training Branch

## Privacy-Driven Speech Transformation

Transforms sppech to a version which is free of personal, private information. 

Inspired by the [Voice Transformer] https://gitlab.inria.fr/comprise/voice_transformation/-/tree/usage/examples/comprise_use_case developed within the COMPRISE project.

Sends the text to the personalized server to be processed, it will return the resulting, privacy-transformed answer.

### transformPrivateSpeech

Transforms speech to a version which is free of personal, private information: 

Find more information in [D2.1](https://www.compriseh2020.eu/files/2019/08/D2.1.pdf)
 

#### Parameters

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **http**  | **any**  | The [native HTTP Plugin from Android or iOS](https://github.com/silkimen/cordova-plugin-advanced-http).  | 
| **dataURI?**  | **string**  | The base64 audio string to transform. This is optional because using the speech to text functionality will already store the last audio file, but this only exists for compatibility reasons and should be considered deprecated. Not using dataURI will also show big issues when the personal server is used by multiple apps and users at the same time  | 


#### Callback

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **transformedAudio**  | **string**  | Base64 string of the transformed audio in WAV  | 

#### Example

``` js
import { transformPrivateSpeech } from 'comprise_privacy_driven_speech_transformation'
import { HTTP } from '@ionic-native/http/ngx';

...

constructor(public http: HTTP) {

	...
	
	transformPrivateSpeech(http).then((transformedAudio) => {
		const audio = new Audio();
		audio.src = "data:audio/wav;base64,"+transformedAudio;
		audio.play();
		console.log("I just played your transformed Audio!");
	},(onerror) => {
		console.log("Following error happened :"+onerror);
	}); 
}

```


## Privacy-Driven Text Transformation

Transforms text to a version which is free of personal, private information. 

Inspired by the [Text Transformer] https://gitlab.inria.fr/comprise/text_transformer developed within the COMPRISE project.

Sends the text to the personalized server to be processed, it will return the resulting, privacy-transformed answer.

### transformText

Transforms text to a version which is free of personal, private information, using one translation strategy of the following:

- REDACT_NE_REPLACEMENT = Deface all private entities, e.g. "Donald Trump" becomes = "XXXXXX XXXXX"
- WORD_BY_WORD_REPLACEMENT = Replace private entities word by word, e.g. "Donald" as is mapped to "John", "Trump" is mapped to "Doe"
- FULL_ENTITY_REPLACEMENT = Replace private entities as complete entity, e.g. "Donald Trump" as a whole is mapped to "John Doe"

Find more information about the differences in [D2.1](https://www.compriseh2020.eu/files/2019/08/D2.1.pdf)
 

#### Parameters

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **text**  | **string**  | The Id of the chosen communication. | 
| **strategy**  | **string**  | The translation strategy to apply, see options above. |  
| **http**  | **any**  | The [native HTTP Plugin from Android or iOS](https://github.com/silkimen/cordova-plugin-advanced-http).  | 

#### Callback

| Name  | Type  | Description
| ------------- | ------------- | ------------- |
| **transformedText**  | **string**  | Text without sensitive information.  | 

#### Example

``` js
import { transformPrivateText, FULL_ENTITY_REPLACEMENT } from 'comprise_privacy_driven_text_transformation'
import { HTTP } from '@ionic-native/http/ngx';

...

constructor(public http: HTTP) {

	...
	
	transformPrivateText(text, FULL_ENTITY_REPLACEMENT, http).then((transformedText) => {
		console.log("I am free of private content: "+transformedText);
	},(onerror) => {
		console.log("Following error happened :"+onerror);
	}); 
}

```

## Personalized Learning

To be documented soon! 

## Cloud Platform

To be documented soon! 

[build-badge]: https://travis-ci.org/maximegris/angular-electron.svg?branch=master&style=style=flat-square
[build]: https://travis-ci.org/maximegris/angular-electron
[license-badge]: https://img.shields.io/badge/license-Apache2-blue.svg?style=style=flat-square
[license]: https://github.com/maximegris/angular-electron/blob/master/LICENSE.md
[prs-badge]: https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square
[prs]: http://makeapullrequest.com
[github-watch-badge]: https://img.shields.io/github/watchers/maximegris/angular-electron.svg?style=social
[github-watch]: https://github.com/maximegris/angular-electron/watchers
[github-star-badge]: https://img.shields.io/github/stars/maximegris/angular-electron.svg?style=social
[github-star]: https://github.com/maximegris/angular-electron/stargazers
[twitter]: https://twitter.com/intent/tweet?text=Check%20out%20COMPRISE%20SDK%20Developer%20UI!%20https://gitlab.inria.fr/comprise/comprise_sdk_developer_ui/-/tree/electron/
[twitter-badge]: https://img.shields.io/twitter/url/https/github.com/maximegris/angular-electron.svg?style=social
[maintained-badge]: https://img.shields.io/badge/maintained-yes-brightgreen

